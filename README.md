# Next_word_prediction_using_LSTM

An LSTM was trained from scratch(using Keras) on Jane Austen's Pride and Prejudice to predict the next word from a sequence of 3 words. After the model was trained it takes in a sentence and uses the last 3 words to predict the next word.

# The dataset to train the next character predictor model
I use the text file that is available in this link->(https://www.gutenberg.org/files/1342/1342-0.txt). I remove some irrelevant information that are present in the initial parts of the text document. 

# Training the model
I prepare a dataset of sentence sequences(X) of size(3 words) and their corresponding next word(Y) and changed it such that an LSTM can learn from the dataset. The LSTM itself that was used a simple neural network with one LSTM layer of 1000 hidden units followed by a densely connected layer with output size equal to the vocubulary of the dataset. Softmax activation was used so as to predict and caculate loss on which character was being predicted for an input sequence. The code to prepare the dataset and train the model is inside the next_word_pred_training.py. 

# Predicting using the trained model
We use a sentence sequence of 3 words and use that as input to the trained model and predict the next word.The code for this prediction is inside inferemce.py. There is an infinite loop that accepts from the user a text sequence of at least 3 words to predict the most probable next word. If a zero is given as input the loop breaks.









