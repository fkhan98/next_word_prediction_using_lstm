from tensorflow.keras.models import load_model
import numpy as np
import pickle

def Predict_Next_Words(model, tokenizer, text):

  sequence = tokenizer.texts_to_sequences([text])
  sequence = np.array(sequence)
  preds = np.argmax(model.predict(sequence))
  predicted_word = ""
  
  for key, value in tokenizer.word_index.items():
      if value == preds:
          predicted_word = key
          break
  
  print(predicted_word)
  return predicted_word


  if __name__ == '__main__':
    model = load_model('next_words.h5')
    tokenizer = pickle.load(open('./token.pkl', 'rb'))
    while(True):
        text = input("Enter your line: ")
        
        if text == "0":
            print("Execution completed.....")
            break
        
        else:
            try:
                text = text.split(" ")
                text = text[-3:]
                print(text)
                
                Predict_Next_Words(model, tokenizer, text)
                
            except Exception as e:
                print("Error occurred: ",e)
                continue

