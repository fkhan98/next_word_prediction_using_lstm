#import tensorflow as tf
from keras.preprocessing.text import Tokenizer
from keras.layers import Embedding, LSTM, Dense
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint
from keras.utils import to_categorical
from keras.optimizers import Adam
import pickle
import numpy as np
import os
from matplotlib import pyplot as plt


def load_file(filepath: str):
    raw_text = open(filepath, 'r', encoding='utf-8').read()
    raw_text = raw_text.lower()
    return raw_text

def preprocess_text(text: str):
    num_removed_text = ''.join(c for c in text if not c.isdigit())
    data = num_removed_text.replace('\n', '').replace('\r', '').replace('\ufeff', '').replace('“','').replace('”','')
    data = data.split()
    data = ' '.join(data)

    return data

def tokenize(data: str):
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts([data])

    # saving the tokenizer for predict function
    pickle.dump(tokenizer, open('token.pkl', 'wb'))

    sequence_data = tokenizer.texts_to_sequences([data])[0]
    vocab_size = len(tokenizer.word_index) + 1
    return sequence_data, vocab_size

def prepare_sequence(tokenized_data: list): #preparing sequences each of size 4
    sequences = []
    for i in range(3, len(tokenized_data)):
        words = tokenized_data[i-3:i+1]
        sequences.append(words)
    return sequences

def prepare_data(sequences):
    X = []
    y = []

    for i in sequences:
        X.append(i[0:3])
        y.append(i[3])
        
    X = np.array(X)
    y = np.array(y)

    return X, y

def get_model(vocab_size, input_length):
    model = Sequential()
    model.add(Embedding(vocab_size, 10, input_length=3))
    model.add(LSTM(1000))
    model.add(Dense(vocab_size, activation="softmax"))

    return model

def visualize_results(history):
    #plot the training accuracy and loss at each epoch
    loss = history.history['loss']
    epochs = range(1, len(loss) + 1)
    plt.plot(epochs, loss, 'y', label='Training loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()

if __name__ == '__main__':
    filename = "./1342-0.txt"
    loaded_file = load_file(filename)
    data = preprocess_text(loaded_file)

    tokenized_data, vocab_size = tokenize(data)
    sequences = prepare_sequence(tokenized_data)

    #print("The Length of sequences are: ", len(sequences))
    sequences = np.array(sequences)

    X, y = prepare_data(sequences)
    print("Data: ", X[:10])
    print("Response: ", y[:10])
    y = to_categorical(y, num_classes=vocab_size)

    model = get_model(vocab_size, 3)
    filepath="saved_weights/saved_weights-{epoch:02d}-{loss:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    model.compile(loss="categorical_crossentropy", optimizer=Adam(learning_rate=0.001))
    history = model.fit(X, y, epochs=50, batch_size=128, callbacks=[checkpoint])

    visualize_results(history)
    model.save('weights_ntp_20epochs.h5')



